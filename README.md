# CONCRETE STRENGTH PREDICTION

The concrete compression strength is a highly nonlinear function of age
and ingredients. These ingredients include cement, blast furnace slag,
fly ash, water, superplasticizer, coarse and fine aggregates.

> I-Cheng Yeh, "**Modeling of strength of high performance concrete using artificial neural networks**". Cement and Concrete Research, Vol. 28, No. 12, pp. 1797-1808 (1998).

## Download the dataset

The dataset is available in Kaggle in the following
[link](https://www.kaggle.com/prathamtripathi/regression-with-neural-networking/download).

Make sure to download the dataset and move the called file
`concrete_data.csv` to the `\input\` folder.

It is highly recommended to download the dataset description too from the
[UCI web page](https://archive.ics.uci.edu/ml/machine-learning-databases/concrete/compressive/Concrete_Readme.txt)
abd move the called file `Concrete_Readme.txt` in the
`\assets\additional\` folder, this file is only for references purposes.
