import matplotlib.pyplot as plt
import numpy as np

def bins_labels(bins, **kwargs):
    '''Plot histogram helper function

    The code was extracted from Stack Overflow, answer by @Pietro Battiston:
    https://stackoverflow.com/questions/23246125/how-to-center-labels-in-histogram-plot

    Parameters
    ----------
    bins : list from start to end by given steps
        description -> The xticks to fit.
        format -> range(start, end, step)
        options -> No apply
    '''
    bin_w = (max(bins) - min(bins)) / (len(bins) - 1)

    try:
        plt.xticks(
            np.arange(min(bins) + bin_w/2, max(bins), bin_w),
            bins,
            **kwargs
            )
    except:
        pass

    plt.xlim(bins[0], bins[-1])

def plot_histogram(dataframe, header, bins=range(0, 380, 10)):
    '''Plot custom histogram

    Parameters
    ----------
    dataframe : Pandas Dataframe
        description -> The dataframe with the attribute to plot
        format -> Quantitative attribute
        options -> No apply

    header : String
        description -> The attribute to plot
        format -> 'header_name'
        options -> No apply

    bins : List from start to end by given steps
        description -> The xticks to fit.
        format -> range(start, end, step)
        options -> No apply
    '''
    fig = plt.figure(figsize=(15,10))
    ax = fig.add_subplot(111)
    plt.hist(dataframe[header], bins=bins, rwidth= 0.9)
    title = plt.title(f'"{header}" attribute histogram')
    title.set_color('gray')
    title.set_size(14)
    label_y = plt.ylabel('Frequency')
    label_y.set_color('gray')
    label_y.set_size(12)
    label_x = plt.xlabel('Values')
    label_x.set_color('gray')
    label_x.set_size(12)
    plt.xticks(list(bins))
    bins_labels(bins, fontsize=10, color='gray')
    ax.tick_params(axis='x', colors='gray')
    ax.tick_params(axis='y', colors='gray')
    plt.axis()
    plt.show();

def preprocess(dataframe, min_data, scaler_object):
    '''Make the data processing steps in new datasets

    Parameters
    ----------
    dataframe : Pandas Dataframe
        description -> The raw data to process
        format -> Original headers and data type
                  of "concrete_data.csv"
        options -> No apply

    min_data : Pandas Series
        description -> Values to use in the 
                       logarithmic transform
        format -> Pandas Series indicating 
                  the header and min value
                  Example: "Cement": 123.0
        options -> No apply

    scaler_object : Sklearn standard scaler object
        description -> StandardScaler fit to 
                       training dataset
        format -> No apply
        options -> No apply

    Returns
    -------
    processed_data : Numpy matrix
        description -> The cleaned and processed data
        format -> No apply
        options -> No apply
    '''
    # Create the new attributes
    dataframe['Cement Ratio'] = dataframe['Cement']/dataframe['Water']
    dataframe['Aggregates Ratio'] = dataframe['Coarse Aggregate'] \
                                    /dataframe['Fine Aggregate']

    # Do the logarithmic transformation
    attributes_to_transform = list(dict(min_data).keys())
    dataframe[attributes_to_transform] = \
        dataframe[attributes_to_transform].apply(
            lambda x: np.log10(x + 1 - min_data),
            axis=1
        )

    # Drop unnecessary attributes
    dataframe.drop(
    columns=['Cement', 'Coarse Aggregate', 'Fine Aggregate'],
    inplace=True
    )

    # Do the standard scaling
    processed_data = scaler_object.transform(dataframe)

    return processed_data